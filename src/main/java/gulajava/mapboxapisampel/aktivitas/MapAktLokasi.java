package gulajava.mapboxapisampel.aktivitas;

import android.Manifest;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationListener;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.views.MapView;

import java.util.ArrayList;
import java.util.List;

import gulajava.mapboxapisampel.R;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Gulajava Ministudio on 2/4/16.
 */
public class MapAktLokasi extends AppCompatActivity
        implements EasyPermissions.PermissionCallbacks {


    private Location mLocationSaya;
    private Location mLocationSayaUpdate;
    private LocationServices mLocationServices;
    private LokasiListener mLokasiListener;
    private MapView mMapView;
    private boolean isInternet = false;
    private boolean isNetGPS = false;
    private UtilNetGPS mUtilNetGPS;
    private Marker mMarkerSaya;


    private static final int KODE_LOKASIPERMISI = 989;
    private String[] permisis = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    private boolean isPermisiOk = false;
    private List<MarkerOptions> mMarkerList;

    private boolean isLokasiSayaTrack = false;

    private Toolbar mToolbar;
    private ActionBar mActionBar;

    //-6.895036, 107.612960


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.halamanutama);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        MapAktLokasi.this.setSupportActionBar(mToolbar);
        mActionBar = MapAktLokasi.this.getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setTitle(R.string.app_name);
        mActionBar.setSubtitle(R.string.app_namesub);

        //inisialisasi maps
        mMapView = (MapView) findViewById(R.id.petamaps);
        mMapView.setAccessToken(MapAktLokasi.this.getString(R.string.apikeymapbox));
        mMapView.setStyleUrl(Style.MAPBOX_STREETS);
        mMapView.setLatLng(new LatLng(-6.895036, 107.612960));
        mMapView.setZoom(11);
        mMapView.setZoomEnabled(true);
//        mMapView.setZoomControlsEnabled(true);
        mMapView.setAllGesturesEnabled(true);
        mMapView.setTiltEnabled(true);
        mMapView.onCreate(savedInstanceState);

        mUtilNetGPS = new UtilNetGPS(MapAktLokasi.this);
        mLokasiListener = new LokasiListener();

        mMarkerList = new ArrayList<>();
        mLocationSaya = new Location("");
        mLocationSaya.setLatitude(-6.895036);
        mLocationSaya.setLongitude(107.612960);

        cekLokasiInternet();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }


    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();

        try {
            mLocationServices.removeLocationListener(mLokasiListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MapAktLokasi.this.getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {

            case R.id.action_lokasisayatrack:

                if (isPermisiOk) {
                    isLokasiSayaTrack = true;
                    tampilMarkerSaya(mLocationSayaUpdate, "Lokasi saya tracks");
                }

                return true;

            case R.id.tampil_markerbanyak:

                if (isPermisiOk) {
                    TaskListLokasiSusun taskListLokasiSusun = new TaskListLokasiSusun();
                    taskListLokasiSusun.execute();
                }

                return true;

            case R.id.peta_jalan:

                if (isPermisiOk) {
                    mMapView.setStyle(Style.MAPBOX_STREETS);
                }

                return true;

            case R.id.peta_emerald:

                if (isPermisiOk) {
                    mMapView.setStyle(Style.EMERALD);
                }
                return true;

            case R.id.peta_hitam:

                if (isPermisiOk) {
                    mMapView.setStyle(Style.DARK);
                }
                return true;

            case R.id.peta_putih:

                if (isPermisiOk) {
                    mMapView.setStyle(Style.LIGHT);
                }
                return true;

            case R.id.satelit:

                if (isPermisiOk) {
                    mMapView.setStyle(Style.SATELLITE);
                }
                return true;

            case R.id.satelitjalan:

                if (isPermisiOk) {
                    mMapView.setStyle(Style.SATELLITE_STREETS);
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void cekLokasiInternet() {

        isInternet = mUtilNetGPS.cekStatusInternet();
        isNetGPS = mUtilNetGPS.cekStatusNetworkGPS();

        if (isInternet && isNetGPS) {
            cekPermisiLokasi();
        } else {
            Toast.makeText(MapAktLokasi.this, "Internet dan akses GPS tidak diaktifkan", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onPermissionsGranted(List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(List<String> perms) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //masukkan ke dalam easypermission
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, MapAktLokasi.this);
    }


    //CEK PERMISI DULU AMBIL LOKASI
    @AfterPermissionGranted(KODE_LOKASIPERMISI)
    private void cekPermisiLokasi() {

        if (EasyPermissions.hasPermissions(MapAktLokasi.this, permisis)) {

            //sudah ada permisi, ambil lokasi dan setel flags
            isPermisiOk = true;

            inisialisasiLokasiListener();
        } else {

            isPermisiOk = false;
            //ga punya permisi, minta dulu
            EasyPermissions.requestPermissions(MapAktLokasi.this, MapAktLokasi.this.getResources().getString(R.string.rasionalokasi), KODE_LOKASIPERMISI,
                    permisis);
        }
    }


    protected class LokasiListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {

            if (location != null) {
                mLocationSayaUpdate = location;
            }

            if (isLokasiSayaTrack) {

                tampilMarkerSaya(mLocationSayaUpdate, "posisi ku awal update");
            }
        }
    }


    //INISIALISASI LOKASI SERVIS
    private void inisialisasiLokasiListener() {

        if (isPermisiOk) {

            mLocationServices = LocationServices.getLocationServices(MapAktLokasi.this);
            mLocationSayaUpdate = mLocationServices.getLastLocation();

            mLocationServices.toggleGPS(true);
            mLocationServices.addLocationListener(mLokasiListener);

            if (mLocationSaya != null) {
                tampilMarkerSaya(mLocationSaya, "posisi ku awal nih");
            }
        }
    }


    //TAMPILKAN MARKER SAYA
    private void tampilMarkerSaya(Location location, String pesan) {

        if (mMarkerSaya != null) {
            mMarkerSaya.remove();
        }

        double lat = location.getLatitude();
        double longs = location.getLongitude();

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(lat, longs, 14));
        markerOptions.title("Lokasi Ku");
        markerOptions.snippet(pesan);
        mMarkerSaya = mMapView.addMarker(markerOptions);

        CameraPosition cameraPosition = new CameraPosition(
                new LatLng(lat, longs),
                14,
                0,
                0
        );
        mMapView.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }


    protected class TaskListLokasiSusun extends AsyncTask<Void, Void, List<MarkerOptions>> {


        @Override
        protected List<MarkerOptions> doInBackground(Void... voids) {

            List<MarkerOptions> markerOptionsList = new ArrayList<>();
            List<LatLng> latLngList = mUtilNetGPS.getListLokasi();

            int panjangs = latLngList.size();
            for (int i = 0; i < panjangs; i++) {

                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLngList.get(i));
                markerOptions.title("Lokasi Bandung " + i);
                markerOptions.snippet("Lokasi objek di Bandung");

                markerOptionsList.add(markerOptions);
            }

            return markerOptionsList;
        }

        @Override
        protected void onPostExecute(List<MarkerOptions> markerOptionses) {
            super.onPostExecute(markerOptionses);

            mMarkerList = markerOptionses;

            if (isPermisiOk) {

                isLokasiSayaTrack = false;

                mMapView.removeAllAnnotations();
                mMapView.addMarkers(mMarkerList);
                tampilMarkerSaya(mLocationSaya, "posisi ku awal nih");
            }

        }
    }


}
